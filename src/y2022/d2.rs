use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};
pub struct Day2 {
    data: Option<String>,
}

impl Input for Day2 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day2 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut total = 0;
            for line in data.lines() {
                let round = line.split(" ").collect::<Vec<_>>();

                let opponent = round[0];
                let me = round[1];

                match opponent {
                    // Rock v Rock
                    "A" if me == "X" => {
                        total += 1 + 3;
                    }
                    // Rock v Paper
                    "A" if me == "Y" => {
                        total += 2 + 6;
                    }
                    // Rock v scissor
                    "A" if me == "Z" => {
                        total += 3;
                    }
                    // Paper v Rock
                    "B" if me == "X" => {
                        total += 1 + 0;
                    }
                    // Paper v Paper
                    "B" if me == "Y" => {
                        total += 2 + 3;
                    }
                    // Paper v scissor
                    "B" if me == "Z" => {
                        total += 3 + 6;
                    }
                    // Scissor v Rock
                    "C" if me == "X" => {
                        total += 1 + 6;
                    }
                    // Scissor v Paper
                    "C" if me == "Y" => {
                        total += 2 + 0;
                    }
                    // Scissor v scissor
                    "C" if me == "Z" => {
                        total += 3 + 3;
                    }
                    _ => {}
                }
            }
            Ok(total)
        } else {
            Err(String::from("[2022 Day2]: No data found"))
        }
    }

    // Rock: 1
    // Paper: 2
    // Scissors: 3

    // Win: 6
    // Tie: 3
    // Lose 0

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut total = 0;
            for line in data.lines() {
                let round = line.split(" ").collect::<Vec<_>>();

                let opponent = round[0];
                let me = round[1];

                match opponent {
                    // Rock Lose -> Scissor
                    "A" if me == "X" => {
                        total += 3;
                    }
                    // Rock Draw -> Rock
                    "A" if me == "Y" => {
                        total += 1 + 3;
                    }
                    // Rock Win -> Paper
                    "A" if me == "Z" => {
                        total += 2 + 6;
                    }
                    // Paper Lose -> Rock
                    "B" if me == "X" => {
                        total += 1;
                    }
                    // Paper Draw -> Paper
                    "B" if me == "Y" => {
                        total += 2 + 3;
                    }
                    // Paper Win -> Scissor
                    "B" if me == "Z" => {
                        total += 3 + 6;
                    }
                    // Scissor Lose -> Paper
                    "C" if me == "X" => {
                        total += 2;
                    }
                    // Scissor Draw -> Scissor
                    "C" if me == "Y" => {
                        total += 3 + 3;
                    }
                    // Scissor Win -> Rock
                    "C" if me == "Z" => {
                        total += 1 + 6;
                    }
                    _ => {}
                }
            }
            Ok(total)
        } else {
            Err(String::from("[2022 Day2]: No data found"))
        }
    }
}
