use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};
pub struct Day6 {
    data: Option<String>,
}

impl Input for Day6 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day6 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            const WINDOW_SIZE: usize = 4;
            let chars = data.trim().chars().collect::<Vec<_>>();

            let mut start = 0;
            let mut end = start + WINDOW_SIZE;

            while has_duplicates(&chars[start..end]) {
                start += 1;
                end += 1;
            }

            Ok(end as u64)
        } else {
            Err(String::from("[2022 Day6]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            const WINDOW_SIZE: usize = 14;
            let chars = data.trim().chars().collect::<Vec<_>>();

            let mut start = 0;
            let mut end = start + WINDOW_SIZE;

            while has_duplicates(&chars[start..end]) {
                start += 1;
                end += 1;
            }

            Ok(end as u64)
        } else {
            Err(String::from("[2022 Day6]: No data found"))
        }
    }
}

fn has_duplicates(slice: &[char]) -> bool {
    let mut slice = slice.to_vec();
    let d = slice.clone();

    slice.sort();
    slice.dedup();

    d.len() != slice.len()
}
