use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};

pub struct Day1 {
    data: Option<String>,
}

impl Input for Day1 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day1 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut max = 0;

            let mut count = 0;
            for line in data.lines() {
                match line.parse::<u64>() {
                    Ok(calorie_count) => {
                        count += calorie_count;
                    }
                    Err(_) => {
                        if count > max {
                            max = count;
                        }
                        count = 0;
                    }
                }
            }

            Ok(max)
        } else {
            Err("[2022 (Day1)]: No data found".into())
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let mut elves = Vec::new();

            let mut count = 0;
            for line in data.lines() {
                match line.parse::<u64>() {
                    Ok(calorie_count) => {
                        count += calorie_count;
                    }
                    Err(_) => {
                        elves.push(count);
                        count = 0;
                    }
                }
            }
            elves.push(count);

            elves.sort();
            elves.reverse();
            Ok(elves[0] + elves[1] + elves[2])
        } else {
            Err("[2022 (Day1)]: No data found".into())
        }
    }
}
