use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};

pub struct Day8 {
    data: Option<String>,
}

impl Input for Day8 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<usize, usize> for Day8 {
    fn part_one(&mut self) -> Res<usize> {
        if let Some(data) = &self.data {
            let mut width = 0;
            let mut height = 0;

            let trees = data
                .lines()
                .flat_map(|line| {
                    width = line.len();
                    height += 1;

                    line.chars()
                        .map(|c| c.to_digit(10).unwrap())
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>();

            let trees_copy = trees.clone();

            let total = trees.iter().enumerate().fold(0, |acc, (xy, tree)| {
                let x = xy % width;
                let y = xy / width;

                let counts = if (y != 0 && y != height - 1) && (x != 0 && x != width - 1) {
                    let (mut left, mut right, mut top, mut bottom) = (false, false, false, false);

                    for w in 0..width {
                        let ci = width * y + w;
                        let row_tree = trees_copy[ci];

                        if !left && (row_tree >= *tree) && (ci < xy) {
                            left = true;
                        }
                        if !right && (row_tree >= *tree) && (ci > xy) {
                            right = true;
                        }
                    }

                    for h in 0..height {
                        let ci = width * h + x;
                        let col_tree = trees_copy[ci];

                        if !top && (col_tree >= *tree) && (ci < xy) {
                            top = true;
                        }
                        if !bottom && (col_tree >= *tree) && (ci > xy) {
                            bottom = true;
                        }
                    }

                    if left && right && bottom && top {
                        false
                    } else {
                        true
                    }
                } else {
                    false
                };

                if counts {
                    acc + 1
                } else {
                    acc
                }
            });

            Ok((total + (2 * width) + (2 * (height - 2))) as usize)
        } else {
            Err(String::from("[2022 Day8]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<usize> {
        if let Some(data) = &self.data {
            let mut width = 0;
            let mut height = 0;

            let trees = data
                .lines()
                .flat_map(|line| {
                    width = line.len();
                    height += 1;

                    line.chars()
                        .map(|c| c.to_digit(10).unwrap())
                        .collect::<Vec<_>>()
                })
                .collect::<Vec<_>>();

            let mut scenic_scores: Vec<usize> = vec![0; trees.len()];

            (0..trees.len())
                .into_iter()
                .for_each(|i| calculate_scenic_score(width, height, i, &trees, &mut scenic_scores));

            let a = scenic_scores.iter().max().unwrap();
            Ok(*a)
        } else {
            Err(String::from("[2022 Day8]: No data found"))
        }
    }
}

fn calculate_scenic_score(
    width: usize,
    height: usize,
    tree_index: usize,
    trees: &Vec<u32>,
    scenic_scores: &mut Vec<usize>,
) {
    let x = tree_index % width;
    let y = tree_index / width;
    let tree = trees[tree_index];

    let (mut left, mut right, mut bottom, mut top) = (0, 0, 0, 0);

    // left
    if x != 0 {
        let mut left_i: i32 = x as i32 - 1;
        while left_i >= 0 {
            if trees[width * y + left_i as usize] >= trees[tree_index] {
                left += 1;
                break;
            }
            left += 1;
            left_i -= 1;
        }
    }

    // right
    if x != width - 1 {
        let mut right_i = x + 1;
        while right_i < width {
            if trees[width * y + right_i] >= trees[tree_index] {
                right += 1;
                break;
            }
            right += 1;
            right_i += 1;
        }
    }

    // top
    if y != 0 {
        let mut top_i: i32 = y as i32 - 1;
        while top_i >= 0 {
            if trees[width * top_i as usize + x] >= trees[tree_index] {
                top += 1;
                break;
            }
            top += 1;
            top_i -= 1;
        }
    }

    // bottom
    if y != height - 1 {
        let mut bottom_i = y + 1;
        while bottom_i < height {
            if trees[width * bottom_i + x] >= trees[tree_index] {
                bottom += 1;
                break;
            }
            bottom += 1;
            bottom_i += 1;
        }
    }

    scenic_scores[tree_index] = left * right * bottom * top;
}
