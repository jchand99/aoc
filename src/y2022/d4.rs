use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};
pub struct Day4 {
    data: Option<String>,
}

impl Input for Day4 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day4 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total =
                data.lines()
                    .map(|l| l.split(",").collect::<Vec<_>>())
                    .fold(0, |acc, splits| {
                        let l = splits[0]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();
                        let r = splits[1]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();

                        if (l[0] <= r[0] && l[1] >= r[1]) || (r[0] <= l[0] && r[1] >= l[1]) {
                            acc + 1
                        } else {
                            acc
                        }
                    });
            Ok(total)
        } else {
            Err(String::from("[2022 Day4]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total =
                data.lines()
                    .map(|l| l.split(",").collect::<Vec<_>>())
                    .fold(0, |acc, splits| {
                        let l = splits[0]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();
                        let r = splits[1]
                            .split("-")
                            .map(|s| s.parse::<u64>().unwrap())
                            .collect::<Vec<_>>();

                        let mut a = [&l, &r];
                        a.sort();
                        let [l, r] = a;

                        if l[1] >= r[0] {
                            acc + 1
                        } else {
                            acc
                        }
                    });
            Ok(total)
        } else {
            Err(String::from("[2022 Day4]: No data found"))
        }
    }
}
