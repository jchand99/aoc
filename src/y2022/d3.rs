use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};
pub struct Day3 {
    data: Option<String>,
}

impl Input for Day3 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u64, u64> for Day3 {
    fn part_one(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total = data
                .lines()
                .map(|line| line.split_at(line.len() / 2))
                .flat_map(|(left, right)| left.chars().find(|c| right.contains(*c)))
                .fold(0, |acc, c| {
                    if c.is_ascii_lowercase() {
                        acc + (c as u32) - 96
                    } else {
                        acc + (c as u32) - 38
                    }
                });

            Ok(total as u64)
        } else {
            Err(String::from("[2022 Day3]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<u64> {
        if let Some(data) = &self.data {
            let total = data
                .lines()
                .step_by(3)
                .zip(
                    data.lines()
                        .skip(1)
                        .step_by(3)
                        .zip(data.lines().skip(2).step_by(3)),
                )
                .flat_map(|(first, (second, third))| {
                    // TODO: this could possibly be optimized
                    // by sorting the strings and iterating
                    // over the smallest first.
                    first
                        .chars()
                        .find(|c| second.contains(*c) && third.contains(*c))
                })
                .fold(0, |acc, c| {
                    if c.is_ascii_lowercase() {
                        acc + (c as u32) - 96
                    } else {
                        acc + (c as u32) - 38
                    }
                });
            Ok(total as u64)
        } else {
            Err(String::from("[2022 Day3]: No data found"))
        }
    }
}
