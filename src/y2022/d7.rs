use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};
use std::collections::HashMap;

pub struct Day7 {
    data: Option<String>,
}

impl Input for Day7 {
    fn from_file(file: impl AsRef<str>) -> Self {
        let s = match std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref())) {
            Ok(content) => Self {
                data: Some(content),
            },
            Err(err) => {
                println!("{:?}", err);
                Self { data: None }
            }
        };
        s
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

// Had to look at r/adventofcode for inspiration :(
impl Problem<u128, u128> for Day7 {
    fn part_one(&mut self) -> Res<u128> {
        if let Some(data) = &self.data {
            let mut dirs = Vec::new();
            let mut lines = data.lines();
            let total = dir_size(&mut lines, &mut dirs);

            let total = dirs
                .iter()
                .fold(0, |acc, x| if *x <= 100_000 { acc + *x } else { acc });
            Ok(total)
        } else {
            Err(String::from("[2022 Day7]: No data found"))
        }
    }

    fn part_two(&mut self) -> Res<u128> {
        if let Some(data) = &self.data {
            let mut dirs = Vec::new();
            let mut lines = data.lines();
            let total = dir_size(&mut lines, &mut dirs);

            let needed = 30_000_000 - (70_000_000 - total);
            dirs.sort();

            let total = dirs.iter().find(|&x| *x >= needed).unwrap_or(&0);
            Ok(*total)
        } else {
            Err(String::from("[2022 Day7]: No data found"))
        }
    }
}

fn dir_size<'a>(lines: &mut impl Iterator<Item = &'a str>, dirs: &mut Vec<u128>) -> u128 {
    let mut size = 0;

    loop {
        let line = lines.next();
        let line = match line {
            Some(line) => line,
            None => break,
        };

        if line == "$ cd .." {
            break;
        }

        let line = line.split_whitespace().collect::<Vec<_>>();
        match &line[..] {
            ["$", "cd", ..] => size += dir_size(lines, dirs),
            _ => {
                size += line[0].parse::<u128>().unwrap_or(0);
            }
        }
    }
    dirs.push(size);

    size
}
