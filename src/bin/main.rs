use aoc::y2022::{Day1, Day2, Day3, Day4, Day5, Day6, Day7, Day8};
use aoc::{Input, Problem};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let mut d = Day1::from_file(format!("2022/{}", "d1.txt"));
    // let solution = d.solve()?;

    // let mut d = Day2::from_file(format!("2022/{}", "d2.txt"));
    // let solution = d.solve()?;

    // let mut d = Day3::from_file(format!("2022/{}", "d3.txt"));
    // let solution = d.solve()?;

    // let mut d = Day4::from_file(format!("2022/{}", "d4.txt"));
    // let solution = d.solve()?;

    // let mut d = Day5::from_file(format!("2022/{}", "d5.txt"));
    // let solution = d.solve()?;

    // let mut d = Day6::from_file(format!("2022/{}", "d6.txt"));
    // let solution = d.solve()?;

    // let mut d = Day7::from_file(format!("2022/{}", "d7.txt"));
    // let solution = d.solve()?;

    let mut d = Day8::from_file(format!("2022/{}", "d8.txt"));
    let solution = d.solve()?;

    println!("{:?}", solution);
    Ok(())
}
