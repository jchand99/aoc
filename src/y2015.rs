use crate::{Input, Problem, Res, INPUT_DATA_DIR, TEST_DATA_DIR};

pub struct Day1 {
    data: Option<String>,
}

impl Input for Day1 {
    fn from_file(file: impl AsRef<str>) -> Self {
        if let Ok(content) =
            std::fs::read_to_string(format!("{}/{}", INPUT_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }

    fn from_sample(file: impl AsRef<str>) -> Self {
        if let Ok(content) = std::fs::read_to_string(format!("{}/{}", TEST_DATA_DIR, file.as_ref()))
        {
            Self {
                data: Some(content),
            }
        } else {
            Self { data: None }
        }
    }
}

impl Problem<u128, u128> for Day1 {
    fn part_one(&mut self) -> Res<u128> {
        if let Some(data) = &self.data {
            let mut level: i64 = 0;
            for c in data.as_str().chars() {
                match c {
                    '(' => level += 1,
                    ')' => level -= 1,
                    _ => {}
                }
            }

            Ok(level as u128)
        } else {
            Err("[2015 (Day1)]: No data found".into())
        }
    }

    fn part_two(&mut self) -> Res<u128> {
        if let Some(data) = &self.data {
            let mut level = 0;
            let mut answer = usize::MAX;
            for (i, c) in data.as_str().chars().enumerate() {
                match c {
                    '(' => level += 1,
                    ')' => level -= 1,
                    _ => {}
                }

                if level < 0 {
                    if i + 1 < answer {
                        answer = i + 1;
                    }
                }
            }

            Ok(answer as u128)
        } else {
            Err("[2015 (Day1)]: No data found".into())
        }
    }
}
